package com.example.rauls.proyectoasee;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.rauls.proyectoasee.Activities.PartidosActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class PartidosTest {
    @Rule
    public ActivityTestRule<PartidosActivity> mActivityRule = new ActivityTestRule<>(PartidosActivity.class);

    @Test
    public void insertarNuevoPartido() {
        //Texto de prueba de equipo local
        String local = "Local";
        //Texto de prueba de equipo visitante
        String visitante = "Visitante";
        //En la pantalla principal, se pulsa sobre el boton de accion flotante para añadir un nuevo partido
        onView(withId(R.id.fab)).perform(click());
        //Se introduce el texto de prueba en el equipo local
        onView(withId(R.id.localTeam_editText)).perform(typeText(local), closeSoftKeyboard());
        //Se introduce el texto de prueba en el equipo visitante
        onView(withId(R.id.visitorTeam_editText)).perform(typeText(visitante), closeSoftKeyboard());
        //Se pulsa sobre el boton de crear para añadir el partido introducido
        onView(withId(R.id.submit_button)).perform(click());
        //Se comprueba que efectivamente existe dentro de la RecyclerView un elemento con un TextView (concretamente el correspondiente al  equipo local)
        onView(withId(R.id.recycler_view_partidos)).check(matches(hasDescendant(withId(R.id.equipoLocal_textView))));
        //Se comprueba que dicho elemento tiene como texto el introducido anteriormente
        onView(withId(R.id.recycler_view_partidos)).check(matches(hasDescendant(withText(local))));
        //Se comprueba que efectivamente existe dentro de la RecyclerView un elemento con un TextView (concretamente el correspondiente al  equipo visitante)
        onView(withId(R.id.recycler_view_partidos)).check(matches(hasDescendant(withId(R.id.equipoVisitante_textView))));
        //Se comprueba que dicho elemeto tiene como texto el introducido anteriormente
        onView(withId(R.id.recycler_view_partidos)).check(matches(hasDescendant(withText(visitante))));
        //Se elimina el partido introducido
        onView(withId(R.id.delete_button)).perform(click());
        //Se comprueba que el partido ha sido borrado correctamente
        onView(withId(R.id.equipoLocal_textView)).check(doesNotExist());
    }

    @Test
    public void editarUnPartido (){
        //Texto de prueba para el equipo local
        String local = "Local";
        //Texto de prueba para el equipo  visitante
        String visitante = "Visitante";
        //Texto de prueba para editar el equipo local
        String nLocal = "Nuevo Local";
        //Texto de prueba para editar el equipo visitante
        String nVisitante = "Nuevo Visitante";
        //Se pulsa sobre el botón de accion flotante para añadir un nuevo partido
        onView(withId(R.id.fab)).perform(click());
        //Se introduce el equipo local
        onView(withId(R.id.localTeam_editText)).perform(typeText(local), closeSoftKeyboard());
        //Se introduce el equipo visitante
        onView(withId(R.id.visitorTeam_editText)).perform(typeText(visitante), closeSoftKeyboard());
        //Se pulsa sobre el boton de crear para añadirlo a la lista
        onView(withId(R.id.submit_button)).perform(click());
        //Se comprueba que existe en la lista un elemento TextView (concretamente el del equipo local)
        onView(withId(R.id.recycler_view_partidos)).check(matches(hasDescendant(withId(R.id.equipoLocal_textView))));
        //Se comprueba que dicho elemento tiene el texto introducido anteriormente
        onView(withId(R.id.recycler_view_partidos)).check(matches(hasDescendant(withText(local))));
        //Se comprueba que existe en la lista un elemento TextView (concretamente el del equipo visitante)
        onView(withId(R.id.recycler_view_partidos)).check(matches(hasDescendant(withId(R.id.equipoVisitante_textView))));
        //Se comprueba que dicho elemento tiene el texto introducido anteriormente
        onView(withId(R.id.recycler_view_partidos)).check(matches(hasDescendant(withText(visitante))));
        //Se pulsa sobre el boton de editar del partido
        onView(withId(R.id.edit_button)).perform(click());
        //Se edita el equipo local con un nuevo texto
        onView(withId(R.id.localTeam_editText)).perform(clearText(), typeText(nLocal), closeSoftKeyboard());
        //Se edita el equipo visitante con un nuevo texto
        onView(withId(R.id.visitorTeam_editText)).perform(clearText(), typeText(nVisitante), closeSoftKeyboard());
        //Se pulsa sobre el boton de guardar cambios
        onView(withId(R.id.submit_button)).perform(click());
        //Se comprueba que en la RecyclerView existe un elemento TextView (concretamente el correspondiente al equipo local)
        onView(withId(R.id.recycler_view_partidos)).check(matches(hasDescendant(withId(R.id.equipoLocal_textView))));
        //Se compueba que dicho elemento tiene como texto el nuevo que se acaba de introducir
        onView(withId(R.id.recycler_view_partidos)).check(matches(hasDescendant(withText(nLocal))));
        //Se comprueba que en la RecyclerView existe un elemento TextView (concretamente el correspondiente al equipo visitante)
        onView(withId(R.id.recycler_view_partidos)).check(matches(hasDescendant(withId(R.id.equipoVisitante_textView))));
        //Se comrpueba que dicho elemento tiene como texto el nuevo que se acaba de introducir
        onView(withId(R.id.recycler_view_partidos)).check(matches(hasDescendant(withText(nVisitante))));
        //Se pulsa sobre el boton de borrar el partido para eliiminarlo
        onView(withId(R.id.delete_button)).perform(click());
        //Se comprueba que el partido ha sido borrado correctamente
        onView(withId(R.id.equipoLocal_textView)).check(doesNotExist());
    }
}
