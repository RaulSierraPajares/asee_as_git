package com.example.rauls.proyectoasee.Activities;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.example.rauls.proyectoasee.R;

public class SettingsActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
    }
}