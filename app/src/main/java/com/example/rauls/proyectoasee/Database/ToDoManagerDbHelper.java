package com.example.rauls.proyectoasee.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ToDoManagerDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "matches.db";


    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_TODOS =
            "CREATE TABLE " + DBContract.Partido.TABLE_NAME + " (" +
                    DBContract.Partido._ID + " INTEGER PRIMARY KEY," +
                    DBContract.Partido.COLUMN_LOCAL_TEAM + TEXT_TYPE + COMMA_SEP +
                    DBContract.Partido.COLUMN_VISITOR_TEAM + TEXT_TYPE + COMMA_SEP +
                    DBContract.Partido.COLUMN_DATE_MATCH + TEXT_TYPE + COMMA_SEP +
                    DBContract.Partido.COLUMN_HOUR_MATCH + TEXT_TYPE + COMMA_SEP +
                    DBContract.Partido.COLUMN_MINUTE_MATCH + TEXT_TYPE +
                     " )";

    private static final String SQL_DELETE_TODOS =
            "DROP TABLE IF EXISTS " + DBContract.Partido.TABLE_NAME;

    public ToDoManagerDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TODOS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_TODOS);
        onCreate(db);
    }
}
