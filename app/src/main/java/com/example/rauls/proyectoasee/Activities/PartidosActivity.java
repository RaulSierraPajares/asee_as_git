package com.example.rauls.proyectoasee.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.rauls.proyectoasee.Adapters.MatchesAdapter;
import com.example.rauls.proyectoasee.R;
import com.example.rauls.proyectoasee.RoomDatabase.Partidos.Partido;
import com.example.rauls.proyectoasee.ViewModel.PartidoViewModel;

import java.util.List;

public class PartidosActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private MatchesAdapter mAdapter;
    private TextView mTextView;
    private NavigationView list;
    private DrawerLayout d;

    private PartidoViewModel mPartidoViewModel;

    public static final int ADD_MATCH_REQUEST = 0;
    public static final int EDIT_MATCH_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partidos);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.pelota1);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        PreferenceManager.setDefaultValues(this, R.xml.settings, false);

        list = (NavigationView) findViewById(R.id.nav_view2);
        d = (DrawerLayout) findViewById(R.id.drawer_layout2);
        d.setBackgroundColor(ContextCompat.getColor(this, R.color.text_color_primary));
        list.setNavigationItemSelectedListener(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (PartidosActivity.this, AddMatchesActivity.class);
                startActivityForResult(intent, ADD_MATCH_REQUEST);
            }
        });

        mTextView = (TextView) findViewById(R.id.no_matches_textView);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_partidos);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);


        mPartidoViewModel = ViewModelProviders.of(this).get(PartidoViewModel.class);
        mAdapter = new MatchesAdapter(this, new MatchesAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Partido partido) { }
        }, mPartidoViewModel);

        mRecyclerView.setAdapter(mAdapter);
        mPartidoViewModel.getmAllPartidos().observe(this, new Observer<List<Partido>>() {
            @Override
            public void onChanged(@Nullable List<Partido> partidos) {
                mAdapter.load(partidos);
                if(partidos.isEmpty()){
                    mTextView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode == RESULT_OK){
            if(requestCode == ADD_MATCH_REQUEST){
                Partido partido = new Partido(data);
                mTextView.setVisibility(View.INVISIBLE);
                mPartidoViewModel.insert(partido);

            } else {
                if(requestCode == EDIT_MATCH_REQUEST){
                    Partido partido = new Partido (data);
                    mPartidoViewModel.update(partido);
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (d.isDrawerOpen(list)) {
                    d.closeDrawers();
                } else {
                    d.openDrawer(list);
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();

        if (id == R.id.nav_search) {
            Intent intent = new Intent(this, SearchActivity.class);
            startActivity(intent);
        }  else if (id == R.id.nav_manage) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout2);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
