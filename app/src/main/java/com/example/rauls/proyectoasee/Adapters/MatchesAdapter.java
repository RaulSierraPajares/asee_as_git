package com.example.rauls.proyectoasee.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.rauls.proyectoasee.Activities.AddMatchesActivity;
import com.example.rauls.proyectoasee.Activities.PartidosActivity;
import com.example.rauls.proyectoasee.R;
import com.example.rauls.proyectoasee.RoomDatabase.Partidos.PartidosDatabase;
import com.example.rauls.proyectoasee.RoomDatabase.Partidos.Partido;
import com.example.rauls.proyectoasee.ViewModel.PartidoViewModel;

import java.util.ArrayList;
import java.util.List;

public class MatchesAdapter extends RecyclerView.Adapter<MatchesAdapter.ViewHolder> {
    private List<Partido> l_partidos = new ArrayList<Partido>();
    private PartidoViewModel pViewModel;
    Context mContext;

    public interface OnItemClickListener {
        void onItemClick(Partido partido);
    }

    private final OnItemClickListener listener;

    public MatchesAdapter (OnItemClickListener listener){
        this.listener = listener;
    }

    public MatchesAdapter (Context context, OnItemClickListener listener, PartidoViewModel p){
        mContext = context;
        this.listener = listener;
        pViewModel = p;
    }

    @Override
    public MatchesAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                                .inflate(R.layout.partido, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int posI) {
        viewHolder.bind(l_partidos.get(posI), listener);
        viewHolder.getDeleteButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pViewModel.delete(l_partidos.get(viewHolder.getAdapterPosition()));
                l_partidos.remove(viewHolder.getAdapterPosition());
                notifyItemRemoved(viewHolder.getAdapterPosition());
                notifyDataSetChanged();
            }
        });
        viewHolder.getEditButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, AddMatchesActivity.class);
                Log.i("IdentificadorInsercion", String.valueOf(l_partidos.get(posI).getId()));
                Partido.packageIntent2(intent,
                        l_partidos.get(posI).getId(),
                        l_partidos.get(posI).getEquipoLocal(),
                        l_partidos.get(posI).getEquipoVisitante(),
                        Partido.FORMAT.format(l_partidos.get(posI).getFechaPartido()),
                        String.valueOf(l_partidos.get(posI).getHora()),
                        String.valueOf(l_partidos.get(posI).getMinutos()));
                ((Activity)mContext).startActivityForResult(intent, PartidosActivity.EDIT_MATCH_REQUEST);
            }
        });

    }

    @Override
    public int getItemCount() {
        return l_partidos.size();
    }

    public void add(Partido partido){
        l_partidos.add(partido);
        notifyDataSetChanged();
    }

    public void clear(){
        l_partidos.clear();
        notifyDataSetChanged();
    }

    public void load (List<Partido> partidos){
        l_partidos.clear();
        l_partidos = partidos;
        notifyDataSetChanged();
    }

    public Object getItem(int pos){
        return l_partidos.get(pos);
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView equipoLocal;
        private TextView equipoVisitante;
        private TextView fechaInicio;
        private TextView horaInicio;
        private Button deleteButton;
        private Button editButton;

        public ViewHolder(View itemView){
            super(itemView);
            equipoLocal = (TextView) itemView.findViewById(R.id.equipoLocal_textView);
            equipoVisitante = (TextView) itemView.findViewById(R.id.equipoVisitante_textView);
            fechaInicio = (TextView) itemView.findViewById(R.id.fecha_textView);
            horaInicio = (TextView) itemView.findViewById(R.id.hora_textView);
            deleteButton = (Button) itemView.findViewById(R.id.delete_button);
            editButton = (Button) itemView.findViewById(R.id.edit_button);
        }

        public void bind(final Partido partido, final OnItemClickListener listener){
            equipoLocal.setText(partido.getEquipoLocal());
            equipoVisitante.setText(partido.getEquipoVisitante());
            fechaInicio.setText(Partido.FORMAT.format(partido.getFechaPartido()));
            horaInicio.setText(partido.horaToString() + ":" + partido.minToString());
        }

        public Button getDeleteButton (){
            return deleteButton;
        }
        public Button getEditButton () {
            return editButton; }


    }

    class AsynDelete extends AsyncTask<Partido, Void, Void>{

        @Override
        protected Void doInBackground(Partido... items) {
            PartidosDatabase db = PartidosDatabase.getDatabase(mContext);
            db.partidoDao().delete(items[0]);
            return null;
        }
    }
}

