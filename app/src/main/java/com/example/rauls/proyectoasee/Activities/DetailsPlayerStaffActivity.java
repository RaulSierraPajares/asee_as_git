package com.example.rauls.proyectoasee.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.example.rauls.proyectoasee.R;
import com.example.rauls.proyectoasee.RoomDatabase.Jugadores.Jugador;
import com.example.rauls.proyectoasee.ViewModel.JugadorViewModel;

public class DetailsPlayerStaffActivity extends AppCompatActivity {
    private JugadorViewModel mJugadorViewModel;

    private TextView name;
    private TextView birth;
    private TextView country;
    private TextView position;
    private TextView nationality;
    private TextView shirt;
    private TextView team;

    private SharedPreferences sp;
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_player_staff);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getSupportActionBar().setDisplayShowHomeEnabled(true);

        name = (TextView) findViewById(R.id.insert_namePL);

        birth = (TextView) findViewById(R.id.insert_date_birth);

        country = (TextView) findViewById(R.id.insert_country);

        position = (TextView) findViewById(R.id.insert_position);

        nationality = (TextView) findViewById(R.id.insert_nationality);

        shirt = (TextView) findViewById(R.id.insert_number);

        team = (TextView) findViewById(R.id.insert_player_team);

        SharedPreferences sp = getSharedPreferences(PreferenceManager.getDefaultSharedPreferencesName(this), MODE_PRIVATE);
        int color = Integer.valueOf(sp.getString("color", "1"));
        setColor(name, color);
        setColor(birth, color);
        setColor(country, color);
        setColor(position, color);
        setColor(nationality, color);
        setColor(shirt, color);
        setColor(team, color);

        String equipo = getIntent().getStringExtra("equipo_nombre");
        String id = getIntent().getStringExtra("identificador");
        if(equipo != null && id != null){
            mJugadorViewModel =  ViewModelProviders.of(this).get(JugadorViewModel.class);
            mJugadorViewModel.getJugador(equipo, id).observe(this, new Observer<Jugador>() {
                @Override
                public void onChanged(@Nullable Jugador jugador) {
                    if(jugador != null){
                        name.setText(jugador.getName());
                        birth.setText(jugador.getDateOfBirth());
                        country.setText(jugador.getCountryOfBirth());
                        position.setText(jugador.getPosition());
                        nationality.setText(jugador.getNationality());
                        shirt.setText(jugador.getShirtNumber());
                        team.setText(jugador.getTeam());

                        getSupportActionBar().setTitle(jugador.getName());
                    }
                }
            });
        }

    }

    private void setColor(TextView v, int c){
        switch (c){
            case 1:
                v.setTextColor(ContextCompat.getColor(this, R.color.transfermarkt));
                break;
            case 2:
                v.setTextColor(ContextCompat.getColor(this, R.color.rojo));
                break;
            case 3:
                v.setTextColor(ContextCompat.getColor(this, R.color.verde));
                break;
            case 4:
                v.setTextColor(ContextCompat.getColor(this, R.color.naranja));
                break;
            default:
                Log.i("Color", "Error al asignar el color elegido por el usuario");
                break;
        }
    }
}
