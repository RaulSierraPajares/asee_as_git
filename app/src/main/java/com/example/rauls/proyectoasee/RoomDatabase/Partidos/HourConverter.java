package com.example.rauls.proyectoasee.RoomDatabase.Partidos;

import android.arch.persistence.room.TypeConverter;

public class HourConverter {
    @TypeConverter
    public static int toHour (String hora) {
        return hora == null ? null : Integer.valueOf(hora);
    }

    @TypeConverter
    public static String toString(int hora){
        return String.valueOf(hora);
    }
}
