package com.example.rauls.proyectoasee.RoomDatabase.Jugadores;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.rauls.proyectoasee.Database.DBContract;

import java.util.List;

@Dao
public interface JugadorDao {
    @Query("SELECT * FROM "+DBContract.Jugador.TABLE_NAME)
    public List<Jugador> getAll();
    @Query("SELECT * FROM "+DBContract.Jugador.TABLE_NAME + " WHERE team" +" = :team")
    public List<Jugador> getAllByTeam(String team);
    @Query("DELETE FROM "+DBContract.Jugador.TABLE_NAME+" WHERE team= :team")
    public void deleteAllByTeam(String team);

    @Query("SELECT * FROM "+DBContract.Jugador.TABLE_NAME+" WHERE team= :team AND id= :id")
    public LiveData<Jugador> getJugador(String team, String id);

    @Insert
    public long insert (Jugador jugador);
    @Update
    public void update (Jugador jugador);
    @Delete
    public void delete (Jugador jugador);
}
