package com.example.rauls.proyectoasee.ManagementAPI;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.rauls.proyectoasee.Fragments.TeamsFragment;
import com.example.rauls.proyectoasee.RoomDatabase.Jugadores.Jugador;
import com.example.rauls.proyectoasee.SimpleTypes.Equipo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NetworkingAPITeamSquad extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new HttpGetTask().execute(getIntent().getStringExtra("id"));
    }

        public static class HttpGetTask extends AsyncTask<String, Void, List<Jugador>> {
        private static final String BASE_URL = "http://api.football-data.org/v2/teams/";
        private Equipo team;

        @Override
        protected List<Jugador> doInBackground(String... strings) {
            try {
                ArrayList<Jugador> players = new ArrayList<>();
                String mParam1 = strings[0];
                URL url = new URL(BASE_URL+mParam1);
                HttpURLConnection myConnection = (HttpURLConnection) url.openConnection();
                myConnection.setRequestProperty("X-Auth-Token", "892fb396431f4038ab1cada8a2dd4fbc");
                if(myConnection.getResponseCode() == 200){
                    JSONObject jsonObject = new JSONObject(TeamsFragment.slurp(myConnection.getInputStream()));
                    String idT, nameT, address, phone, website, email, foundedYear, stadiumName;
                    if(jsonObject.get("id").equals(null)){
                        idT = "No consta";
                    } else {
                        idT = String.valueOf(jsonObject.getLong("id"));
                    }
                    if(jsonObject.get("name").equals(null)){
                        nameT = "No consta";
                    } else {
                        nameT = jsonObject.getString("name");
                    }
                    if(jsonObject.get("address").equals(null)){
                        address = "No consta";
                    } else {
                        address = jsonObject.getString("address");
                    }
                    if(jsonObject.get("phone").equals(null)){
                        phone = "No consta";
                    } else {
                        phone = jsonObject.getString("phone");
                    }
                    if(jsonObject.get("website").equals(null)){
                        website = "No consta";
                    } else {
                        website = jsonObject.getString("website");
                    }
                    if(jsonObject.get("email").equals(null)){
                        email = "No consta";
                    } else {
                        email = jsonObject.getString("email");
                    }
                    if(jsonObject.get("founded").equals(null)){
                        foundedYear = "No consta";
                    } else {
                        foundedYear = String.valueOf(jsonObject.getInt("founded"));
                    }
                    if(jsonObject.get("venue").equals(null)){
                        stadiumName = "No consta";
                    } else {
                        stadiumName = jsonObject.getString("venue");
                    }
                    JSONArray jsonArray = jsonObject.getJSONArray("squad");
                    Date date = new Date();
                    for(int i = 0; i<jsonArray.length(); i++){
                        JSONObject jsonObjectChild = jsonArray.getJSONObject(i);
                        String id, name, dateOfBirth, countryOfBirth, nationality, position, shirtNumber;
                        if(jsonObjectChild.get("id").equals(null)){
                            id = "No consta";
                        } else {
                            id = String.valueOf(jsonObjectChild.getLong("id"));
                        }
                        if(jsonObjectChild.get("name").equals(null)){
                            name = "No consta";
                        } else {
                            name =jsonObjectChild.getString("name");
                        }
                        if(jsonObjectChild.get("dateOfBirth").equals(null)){
                            dateOfBirth = "No consta";
                        } else {
                            dateOfBirth =jsonObjectChild.getString("dateOfBirth");
                        }
                        if(jsonObjectChild.get("countryOfBirth").equals(null)){
                            countryOfBirth = "No consta";
                        } else {
                            countryOfBirth =jsonObjectChild.getString("countryOfBirth");
                        }
                        if(jsonObjectChild.get("nationality").equals(null)){
                            nationality = "No consta";
                        } else {
                            nationality =jsonObjectChild.getString("nationality");
                        }
                        if(jsonObjectChild.get("position").equals(null)){
                            position = "No consta";
                        } else {
                            position =jsonObjectChild.getString("position");
                        }
                        if(jsonObjectChild.get("shirtNumber").equals(null)){
                            shirtNumber = "No consta";
                        } else {
                            shirtNumber = String.valueOf(jsonObjectChild.getInt("shirtNumber"));
                        }
                        players.add(new Jugador(id, name, dateOfBirth, countryOfBirth, nationality, position, shirtNumber, nameT, date));
                    }
                    team = new Equipo(idT, nameT, address, phone, website, email, foundedYear, stadiumName, players);
                } else {
                    Log.i("ErrorFicheroJugadores", "No se ha conectado correctamente");
                }
                return players;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                Log.i("ExcepcionJSON", "Excepcion de MalformeURL");
            } catch (IOException e) {
                e.printStackTrace();
                Log.i("ExcepcionJSON", "Excepcion de IOException");
            } catch (JSONException e) {
                Log.i("ExcepcionJSON", e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Jugador> result) {
//            Intent intent = new Intent(NetworkingAPITeamSquad.this, DetailsTeamActivity.class);
//            Bundle bundle = new Bundle();
//            bundle.putSerializable("equipo", team);
//            bundle.putSerializable("jugadores", (Serializable) team.getSquad());
//            intent.putExtra("bundle", bundle);
//            startActivity(intent);
//            finish();
        }
    }
}
