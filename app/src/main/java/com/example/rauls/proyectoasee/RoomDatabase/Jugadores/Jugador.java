package com.example.rauls.proyectoasee.RoomDatabase.Jugadores;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.example.rauls.proyectoasee.Database.DBContract;
import com.example.rauls.proyectoasee.RoomDatabase.Partidos.DateConverter;

import java.io.Serializable;
import java.util.Date;

@Entity(tableName = DBContract.Jugador.TABLE_NAME)
public class Jugador implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private long idDB;
    private String id;
    private String name;
    private String dateOfBirth;
    private String countryOfBirth;
    private String nationality;
    private String position;
    private String shirtNumber;
    private String team;
    @TypeConverters(DateConverter.class)
    private Date fechaAPI;

    public Jugador(long idDB, String id, String name, String dateOfBirth, String countryOfBirth, String nationality, String position, String shirtNumber, String team, Date fechaAPI) {
        this.idDB = idDB;
        this.id = id;
        this.name = name;
        this.dateOfBirth = cortarFecha(dateOfBirth);
        this.countryOfBirth = countryOfBirth;
        this.nationality = nationality;
        this.position = position;
        this.shirtNumber = shirtNumber;
        this.team = team;
        this.fechaAPI = fechaAPI;
    }

    @Ignore
    public Jugador(String id, String name, String dateOfBirth, String countryOfBirth, String nationality, String position, String shirtNumber, String team, Date fechaAPI) {
        this.id = id;
        this.name = name;
        this.dateOfBirth = cortarFecha(dateOfBirth);
        this.countryOfBirth = countryOfBirth;
        this.nationality = nationality;
        this.position = position;
        this.shirtNumber = shirtNumber;
        this.team = team;
        this.fechaAPI = fechaAPI;
    }

    public String etId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getCountryOfBirth() {
        return countryOfBirth;
    }

    public void setCountryOfBirth(String countryOfBirth) {
        this.countryOfBirth = countryOfBirth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getShirtNumber() {
        return shirtNumber;
    }

    public void setShirtNumber(String shirtNumber) {
        this.shirtNumber = shirtNumber;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public long getIdDB() {
        return idDB;
    }

    public void setIdDB(long idDB) {
        this.idDB = idDB;
    }

    public String getId() {
        return id;
    }

    public Date getFechaAPI() {
        return fechaAPI;
    }

    public void setFechaAPI(Date fechaAPI) {
        this.fechaAPI = fechaAPI;
    }

    @Ignore
    public String cortarFecha(String fecha){
        String f = "";
        boolean seguir = true;
        for(int i = 0; i<fecha.length() && seguir; i++){
            if(fecha.toCharArray()[i] != 'T'){
                f = f+fecha.toCharArray()[i];
            } else {
                seguir = false;
            }
        }
        return f;
    }
}