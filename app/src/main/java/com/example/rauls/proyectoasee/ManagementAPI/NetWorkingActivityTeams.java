package com.example.rauls.proyectoasee.ManagementAPI;

import android.app.ListActivity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.example.rauls.proyectoasee.Fragments.TeamsFragment;
import com.example.rauls.proyectoasee.SimpleTypes.Equipo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class NetWorkingActivityTeams extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new HttpGetTask().execute(getIntent().getStringExtra("competition"));
    }

    public static class HttpGetTask extends AsyncTask<String, Void, List<Equipo>> {
        private static final String BASE_URL = "http://api.football-data.org/v2/";
        private ArrayList <Equipo> team_list = new ArrayList<>();
        @Override
        protected List<Equipo> doInBackground(String... strings) {
            Log.i("Paso", "Vamos a obtener informacion de la API: "+strings[0]);
            try {
                String mParam1 = strings[0];
                URL url = new URL(BASE_URL+mParam1);
                HttpURLConnection myConnection = (HttpURLConnection) url.openConnection();
                myConnection.setRequestProperty("X-Auth-Token", "892fb396431f4038ab1cada8a2dd4fbc");

                if(myConnection.getResponseCode() == 200){
                    JSONObject jsonObject = new JSONObject(TeamsFragment.slurp(myConnection.getInputStream()));
                    JSONArray jsonArray = jsonObject.getJSONArray("teams");
                    for(int i = 0; i<jsonArray.length(); i++){
                        JSONObject jsonObjectChild = jsonArray.getJSONObject(i);
                        String idT, nameT, address, phone, website, email, foundedYear, stadiumName;
                        if(jsonObjectChild.get("id").equals(null)){
                            idT = "No consta";
                        } else {
                            idT = String.valueOf(jsonObjectChild.getLong("id"));
                        }
                        if(jsonObjectChild.get("name").equals(null)){
                            nameT = "No consta";
                        } else {
                            nameT = jsonObjectChild.getString("name");
                        }
                        if(jsonObjectChild.get("address").equals(null)){
                            address = "No consta";
                        } else {
                            address = jsonObjectChild.getString("address");
                        }
                        if(jsonObjectChild.get("phone").equals(null)){
                            phone = "No consta";
                        } else {
                            phone = jsonObjectChild.getString("phone");
                        }
                        if(jsonObjectChild.get("website").equals(null)){
                            website = "No consta";
                        } else {
                            website = jsonObjectChild.getString("website");
                        }
                        if(jsonObjectChild.get("email").equals(null)){
                            email = "No consta";
                        } else {
                            email = jsonObjectChild.getString("email");
                        }
                        if(jsonObjectChild.get("founded").equals(null)){
                            foundedYear = "No consta";
                        } else {
                            foundedYear = String.valueOf(jsonObjectChild.getInt("founded"));
                        }
                        if(jsonObjectChild.get("venue").equals(null)){
                            stadiumName = "No consta";
                        } else {
                            stadiumName = jsonObjectChild.getString("venue");
                        }
                        team_list.add(new Equipo(idT, nameT, address, phone, website, email, foundedYear, stadiumName));
                    }
                } else {
                    Log.i("ErrorFichero", "No se ha conectado correctamente");
                }
                return team_list;
            } catch (MalformedURLException e) {
                e.printStackTrace();
                Log.i("ExcepcionJSON", "Excepcion de MalformeURL");
            } catch (IOException e) {
                e.printStackTrace();
                Log.i("ExcepcionJSON", "Excepcion de IOException");
            } catch (JSONException e) {
                Log.i("ExcepcionJSON", "Excepcion de JSON");
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Equipo> result) {
//            Intent intent = new Intent(NetWorkingActivityTeams.this, SearchActivity.class);
//            Bundle bundle = new Bundle();
//            bundle.putSerializable("lista", team_list);
//            intent.putExtra("bundle", bundle);
//            startActivity(intent);
//            finish();
        }
    }
}
