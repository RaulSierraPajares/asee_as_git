package com.example.rauls.proyectoasee.Activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.rauls.proyectoasee.R;
import com.example.rauls.proyectoasee.RoomDatabase.Partidos.Partido;

import java.util.Calendar;
import java.util.Date;

public class AddMatchesActivity extends AppCompatActivity {
    private static String hourString;
    private static String minString;
    private static String timeString;
    private static String dateString;

    private static int minApp;
    private static int hourApp;
    private static int dayApp;
    private static int monthApp;
    private static int yearApp;

    private static TextView dateView;
    private static TextView timeView;

    private EditText mLocal;
    private EditText mVisitor;
    private Date mDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_matches);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mLocal = (EditText) findViewById(R.id.localTeam_editText);
        mVisitor = (EditText) findViewById(R.id.visitorTeam_editText);
        dateView = (TextView) findViewById(R.id.date_textView);
        timeView = (TextView) findViewById(R.id.time_textView);


        if(getIntent().getStringExtra(Partido.ID)==null){
            setDefaultDateTime();
        }

        final Button datePikerButton = (Button) findViewById(R.id.date_button);
        datePikerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        final Button timePikerButton = (Button) findViewById(R.id.time_button);
        timePikerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog();
            }
        });

        final Button cancelButton = (Button) findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
            }
        });

        final Button submitButton = (Button) findViewById(R.id.submit_button);
        if(getIntent().getStringExtra(Partido.EQUIPOLOCAL)!=null){
            submitButton.setText(R.string.update_match);
        }
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String local = mLocal.getText().toString();
                String visitor = mVisitor.getText().toString();
                if(!local.equals("") && !visitor.equals("") && fechaCorrecta(yearApp, monthApp, dayApp, hourApp, minApp)){
                    Intent intent = new Intent();
                    if(getIntent().getStringExtra(Partido.ID)==null){
                        Partido.packageIntent(intent, local, visitor, dateString, hourString, minString);
                    } else {
                        Partido.packageIntent2(intent, Long.parseLong(getIntent().getStringExtra(Partido.ID)), local, visitor, dateString, hourString, minString);
                    }
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    Snackbar.make(v, "Error al introducir los parámetros", Snackbar.LENGTH_LONG)
                            .show();
                }
            }
        });
    }

    @Override
    protected void onResume(){
        super.onResume();
        if(getIntent().getStringExtra(Partido.EQUIPOLOCAL)!=null){
            mLocal.setText(getIntent().getStringExtra(Partido.EQUIPOLOCAL));
            mVisitor.setText(getIntent().getStringExtra(Partido.EQUIPOVISITANTE));
            dateString = getIntent().getStringExtra(Partido.FECHAINICIO);
            dateView.setText(dateString);
            setTimeString(Integer.valueOf(getIntent().getStringExtra(Partido.HORA)), Integer.valueOf(getIntent().getStringExtra(Partido.MINUTOS)), 0);
            timeView.setText(timeString);
        }
    }

    private void showTimePickerDialog() {
        DialogFragment timePicker = new TimePickerFragment();
        timePicker.show(getFragmentManager(), "timePicker");
    }

    private void showDatePickerDialog() {
        DialogFragment datePicker = new DatePickerFragment();
        datePicker.show(getFragmentManager(), "datePicker");
    }

    private void setDefaultDateTime(){
        mDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(mDate);
        setDateString(c.get(Calendar.YEAR), c.get(Calendar.MONTH),
                c.get(Calendar.DAY_OF_MONTH));

        dateView.setText(dateString);

        setTimeString(c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE),
                c.get(Calendar.MILLISECOND));

        timeView.setText(timeString);
    }

    private static void setDateString(int year, int monthOfYear, int dayOfMonth) {

        // Increment monthOfYear for Calendar/Date -> Time Format setting
        monthOfYear++;
        String mon = "" + monthOfYear;
        String day = "" + dayOfMonth;

        yearApp = year;
        monthApp = monthOfYear;
        dayApp = dayOfMonth;

        if (monthOfYear < 10)
            mon = "0" + monthOfYear;
        if (dayOfMonth < 10)
            day = "0" + dayOfMonth;

        dateString = year + "-" + mon + "-" + day;
    }

    private static void setTimeString(int hourOfDay, int minute, int mili) {
        hourString = "" + hourOfDay;
        minString = "" + minute;

        hourApp = hourOfDay;
        minApp = minute;

        if (hourOfDay < 10)
            hourString = "0" + hourOfDay;
        if (minute < 10)
            minString = "0" + minute;

        timeString = hourString + ":" + minString;
    }

    private boolean fechaCorrecta (int y, int m, int d, int h, int min){
        boolean correcta = false;
        Date actual = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(actual);
        if(y > c.get(Calendar.YEAR)){
            correcta = true;
        } else {
            if(y == c.get(Calendar.YEAR) && m > c.get(Calendar.MONTH)){
                correcta = true;
            } else {
                if(y == c.get(Calendar.YEAR) && m == c.get(Calendar.MONTH) && d > c.get(Calendar.DAY_OF_MONTH)){
                    correcta = true;
                } else {
                    if (y == c.get(Calendar.YEAR) && m == c.get(Calendar.MONTH) && d == c.get(Calendar.DAY_OF_MONTH) &&
                        h > c.get(Calendar.HOUR_OF_DAY)){
                        correcta = true;
                    } else {
                        if(y == c.get(Calendar.YEAR) && m == c.get(Calendar.MONTH) && d == c.get(Calendar.DAY_OF_MONTH) &&
                                h == c.get(Calendar.HOUR_OF_DAY) && min > c.get(Calendar.MINUTE)){
                            correcta = true;
                        }
                    }
                }
            }
        }
        return correcta;
    }

    public static class DatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            // Use the current date as the default date in the picker

            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            setDateString(year, monthOfYear, dayOfMonth);

            dateView.setText(dateString);
        }

    }

    // DialogFragment used to pick a ToDoItem deadline time

    public static class TimePickerFragment extends DialogFragment implements
            TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    true);
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            setTimeString(hourOfDay, minute, 0);

            timeView.setText(timeString);
        }
    }

}
