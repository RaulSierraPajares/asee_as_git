package com.example.rauls.proyectoasee.Activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;

import com.example.rauls.proyectoasee.Fragments.CountryFragment;
import com.example.rauls.proyectoasee.Fragments.TeamsFragment;
import com.example.rauls.proyectoasee.R;

public class SearchActivity extends AppCompatActivity implements CountryFragment.OnFragmentInteractionListener,
        TeamsFragment.OnFragmentInteractionListener,
        SearchView.OnQueryTextListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if(getIntent().getBundleExtra("bundle") != null){
            Bundle args = new Bundle();
            args.putSerializable("lista", getIntent().getBundleExtra("bundle").getSerializable("lista"));
            TeamsFragment tfr = new TeamsFragment();
            tfr.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(R.id.contenedor, tfr).addToBackStack("").commit();
        } else {
            CountryFragment fr = new CountryFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.contenedor, fr).commit();
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {  }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}
