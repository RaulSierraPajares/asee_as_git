package com.example.rauls.proyectoasee.RoomDatabase.Jugadores;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {Jugador.class}, version = 1)
public abstract class JugadorDatabase extends RoomDatabase {
    private static JugadorDatabase instance;

    public static JugadorDatabase getDatabase (Context context){
        if(instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(), JugadorDatabase.class, "jugadores.db").build();
        }
        return instance;
    }

    public abstract JugadorDao jugadorDao();
}
