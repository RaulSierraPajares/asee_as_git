package com.example.rauls.proyectoasee.Database;

import android.provider.BaseColumns;

public final class DBContract {
    private DBContract(){}

    public static class Partido implements BaseColumns{
        public static final String TABLE_NAME = "Partido";
        public static final String COLUMN_LOCAL_TEAM = "EquipoLocal";
        public static final String COLUMN_VISITOR_TEAM = "EquipoVisitante";
        public static final String COLUMN_DATE_MATCH = "FechaDelPartido";
        public static final String COLUMN_HOUR_MATCH ="HoraDelPartido";
        public static final String COLUMN_MINUTE_MATCH = "MinutosDelPartido";
    }

    public static class Equipo implements BaseColumns {
        public static final String TABLE_NAME = "Equipos";
        public static final String COLUMN_NAME = "Nombre";
        public static final String COLUMN_ADDRESS = "Direccion";
        public static final String COLUMN_PHONE = "Telefono";
        public static final String COLUMN_WEB ="Web";
        public static final String COLUMN_EMAIL = "Email";
        public static final String COLUMN_YEAR = "Year";
        public static final String COLUMN_STADIUM = "Estadio";
        public static final String COLUMN_PAIS = "Pais";
    }

    public static class Jugador implements BaseColumns {
        public static final String TABLE_NAME = "Jugadores";
        public static final String COLUMN_NAME = "Nombre";
        public static final String COLUMN_BIRTH = "Fecha de nacimiento";
        public static final String COLUMN_COUNTRY = "Pais de origen";
        public static final String COLUMN_NATIONALITY ="Nacionalidad";
        public static final String COLUMN_POSITION = "Posicion";
        public static final String COLUMN_NUMBER = "Camiseta";
        public static final String COLUMN_TEAM = "Equipo actual";
        public static final String COLUMN_PAIS = "Pais";
    }
}
