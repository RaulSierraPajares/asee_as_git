package com.example.rauls.proyectoasee.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.util.Log;

import com.example.rauls.proyectoasee.ManagementAPI.NetWorkingActivityTeams;
import com.example.rauls.proyectoasee.RoomDatabase.Equipos.EquipoDB;
import com.example.rauls.proyectoasee.RoomDatabase.Equipos.EquipoDao;
import com.example.rauls.proyectoasee.RoomDatabase.Equipos.EquipoDatabase;
import com.example.rauls.proyectoasee.RoomDatabase.Partidos.Partido;
import com.example.rauls.proyectoasee.SimpleTypes.Equipo;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class EquipoRepository {
    private static EquipoRepository instance;
    private EquipoDao mEquipoDao;
    private static LiveData<List<EquipoDB>> mAllEquipos;

    private static List<Equipo> equipos;
    private long umbral =  432000000; //Cinco dias

    public EquipoRepository (Application app){
        EquipoDatabase db = EquipoDatabase.getDatabase(app);
        mEquipoDao = db.equipoDao();
    }

    public static EquipoRepository getRepository(Application app){
        if(instance == null){
            instance = new EquipoRepository(app);
        }
        return instance;
    }


    public LiveData<List<EquipoDB>> getAllEquipos () {
        return mAllEquipos;
    }

    public void obtenerDatos(String pais, String competicion){
        try {
            equipos = new NetWorkingActivityTeams.HttpGetTask().execute(competicion).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<EquipoDB> aux = new ArrayList<>();
        aux = converter(equipos, pais);
        for(int i = 0; i<aux.size(); i++){
            insert(aux.get(i));
        }
    }

    public List<EquipoDB> converter(List<Equipo> equipos, String pais) {
        List<EquipoDB> equiposDB = new ArrayList<>();
        Date date = new Date();
        for(int i = 0; i<equipos.size(); i++){
            Equipo aux = equipos.get(i);
            equiposDB.add(new EquipoDB(aux.getName(), aux.getAddress(), aux.getPhone(), aux.getWebSite(),
                                        aux.getEmail(), aux.getFoundedYear(), aux.getStadiumName(), pais, date, aux.getId()));
        }
        return equiposDB;
    }

    public void insert (EquipoDB equipo){
        new EquipoRepository.insertAsynTask(mEquipoDao).execute(equipo);
    }

    public void update (EquipoDB equipo){
        new EquipoRepository.updateAsynTask(mEquipoDao).execute(equipo);
    }

    public void delete(EquipoDB equipo){
        new EquipoRepository.deleteAsynTask(mEquipoDao).execute(equipo);
    }

    public LiveData<List<EquipoDB>> getAllByCountry(String pais, String competicion){

        try {
            if(new count(mEquipoDao).execute(pais).get() == 0){
                obtenerDatos(pais, competicion);
            } else {
                Date current = new Date();
                if(new fecha(mEquipoDao).execute(pais).get().getTime() - current.getTime() > umbral){
                    new deleteAllCountryAsynTask(mEquipoDao).execute(pais);
                    obtenerDatos(pais, competicion);
                }
            }
            mAllEquipos = new getAllByCountryAsynTask(mEquipoDao, pais).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return mAllEquipos;
    }

    private static class insertAsynTask extends AsyncTask<EquipoDB, Void, Void> {
        private EquipoDao mAsynTaskDao;

        insertAsynTask(EquipoDao dao){
            mAsynTaskDao = dao;
        }

        @Override
        protected Void doInBackground(EquipoDB... items) {
            long id = mAsynTaskDao.insert(items[0]);
            Log.i("ID", ""+id);
            return null;
        }
    }

    private static class updateAsynTask extends AsyncTask<EquipoDB, Void, Void>{
        private EquipoDao mAsynTaskDao;

        updateAsynTask (EquipoDao dao) {
            mAsynTaskDao = dao;
        }
        @Override
        protected Void doInBackground(EquipoDB... items) {
            mAsynTaskDao.update(items[0]);
            return null;
        }
    }

    private static class deleteAsynTask extends AsyncTask<EquipoDB, Void, Void>{
        private EquipoDao mAsynTaskDao;

        deleteAsynTask(EquipoDao dao){
            mAsynTaskDao = dao;
        }

        @Override
        protected Void doInBackground(EquipoDB... items) {
            mAsynTaskDao.delete(items[0]);
            return null;
        }
    }

    private static class deleteAllCountryAsynTask extends AsyncTask<String, Void, Void>{
        private EquipoDao mAsynTaskDao;

        deleteAllCountryAsynTask(EquipoDao dao){
            mAsynTaskDao = dao;
        }

        @Override
        protected Void doInBackground(String... items) {
            mAsynTaskDao.deleteAllCountry(items[0]);
            return null;
        }
    }

    private static class getAllByCountryAsynTask extends AsyncTask<Void, Void, LiveData<List<EquipoDB>>>{
        private EquipoDao mAsynTaskDao;
        private String pais;

        getAllByCountryAsynTask (EquipoDao dao, String pais){
            mAsynTaskDao = dao;
            this.pais = pais;
        }

        @Override
        protected LiveData<List<EquipoDB>> doInBackground(Void... voids) {
            LiveData<List<EquipoDB>> aux = mAsynTaskDao.getAllByCountry(pais);
            return aux;
        }
    }

    private static class getAllAsynTask extends AsyncTask<Void, Void, LiveData<List<EquipoDB>>>{
        private EquipoDao mAsynTaskDao;

        getAllAsynTask (EquipoDao dao){
            mAsynTaskDao = dao;
        }

        @Override
        protected LiveData<List<EquipoDB>> doInBackground(Void... voids) {
            LiveData<List<EquipoDB>> aux = mAsynTaskDao.getAll();
            return aux;
        }
    }

    private static class count extends AsyncTask<String, Void, Long>{
        private EquipoDao mAsynTaskDao;

        count(EquipoDao dao){
            mAsynTaskDao = dao;
        }

        @Override
        protected Long doInBackground(String... items) {
            return mAsynTaskDao.count(items[0]);
        }
    }

    private static class fecha extends AsyncTask<String, Void, Date>{
        private EquipoDao mAsynTaskDao;

        fecha (EquipoDao dao){
            mAsynTaskDao = dao;
        }

        @Override
        protected Date doInBackground(String... strings) {
            try {
                return Partido.FORMAT.parse(mAsynTaskDao.fecha(strings[0]));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
