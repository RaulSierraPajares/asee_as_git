package com.example.rauls.proyectoasee.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.rauls.proyectoasee.Repository.PartidoRepository;
import com.example.rauls.proyectoasee.RoomDatabase.Partidos.Partido;

import java.util.List;

public class PartidoViewModel extends AndroidViewModel {
    private PartidoRepository mRepository;

    private LiveData<List<Partido>> mAllPartidos;

    public PartidoViewModel(@NonNull Application application) {
        super(application);
        mRepository = new PartidoRepository(application);
        mAllPartidos = mRepository.getAllPartidos();
    }

    public LiveData<List<Partido>> getmAllPartidos() {
        return mAllPartidos;
    }

    public void insert(Partido partido){
        mRepository.insert(partido);
    }

    public void update(Partido partido){
        mRepository.update(partido);
    }

    public void delete(Partido partido){
        mRepository.delete(partido);
    }

}
