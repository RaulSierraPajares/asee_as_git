package com.example.rauls.proyectoasee.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.rauls.proyectoasee.RoomDatabase.Partidos.PartidoDao;
import com.example.rauls.proyectoasee.RoomDatabase.Partidos.PartidosDatabase;
import com.example.rauls.proyectoasee.RoomDatabase.Partidos.Partido;

import java.util.List;

public class PartidoRepository {
    private PartidoDao mPartidoDao;
    private LiveData<List<Partido>> mAllPartidos;

    public PartidoRepository(Application app){
        PartidosDatabase db = PartidosDatabase.getDatabase(app);
        mPartidoDao  = db.partidoDao();
        mAllPartidos = mPartidoDao.getAll();
    }

    public LiveData<List <Partido>> getAllPartidos(){
        return mAllPartidos;
    }

    public void insert (Partido partido){
        new insertAsynTask(mPartidoDao).execute(partido);
    }

    public void update (Partido partido){
        new updateAsynTask(mPartidoDao).execute(partido);
    }

    public void delete(Partido partido){
        new deleteAsynTask(mPartidoDao).execute(partido);
    }

    private static class insertAsynTask extends AsyncTask<Partido, Void, Void> {
        private PartidoDao mAsynTaskDao;

        insertAsynTask(PartidoDao dao){
            mAsynTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Partido... partidos) {
            mAsynTaskDao.insert(partidos[0]);
            return null;
        }
    }

    private static class updateAsynTask extends AsyncTask<Partido, Void, Void>{
        private PartidoDao mAsynTaskDao;

        updateAsynTask (PartidoDao dao) {
            mAsynTaskDao = dao;
        }
        @Override
        protected Void doInBackground(Partido... items) {
            mAsynTaskDao.update(items[0]);
            return null;
        }
    }

    private static class deleteAsynTask extends AsyncTask<Partido, Void, Void>{
        private PartidoDao mAsynTaskDao;

        deleteAsynTask(PartidoDao dao){
            mAsynTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Partido... items) {
            mAsynTaskDao.delete(items[0]);
            return null;
        }
    }


}
