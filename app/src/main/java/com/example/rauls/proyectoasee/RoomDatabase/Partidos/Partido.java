package com.example.rauls.proyectoasee.RoomDatabase.Partidos;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.content.Intent;

import com.example.rauls.proyectoasee.Database.DBContract;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Entity(tableName = DBContract.Partido.TABLE_NAME)
public class Partido {
    @Ignore
    public static final String ITEM_SEP = System.getProperty("line.separator");
    @Ignore
    public final static String ID = "ID";
    @Ignore
    public final static String EQUIPOLOCAL = "equipoLocal";
    @Ignore
    public final static String EQUIPOVISITANTE = "equipoVisitante";
    @Ignore
    public final static String FECHAINICIO = "fechaInicio";
    @Ignore
    public final static String HORA = "hora";
    @Ignore
    public final static String MINUTOS = "minutos";
    @Ignore
    public final static SimpleDateFormat FORMAT = new SimpleDateFormat(
            "yyyy-MM-dd", Locale.US);

    @PrimaryKey(autoGenerate = true)
    private long id;
    private String equipoLocal;
    private String equipoVisitante;
    @TypeConverters(DateConverter.class)
    private Date fechaPartido;
    @TypeConverters(HourConverter.class)
    private int hora;
    @TypeConverters(MinConverter.class)
    private int minutos;

    @Ignore
    public Partido (String equipoLocal, String equipoVisitante, Date fechaPartido, int hora, int minutos){
        this.equipoLocal = equipoLocal;
        this.equipoVisitante = equipoVisitante;
        this.fechaPartido = fechaPartido;
        this.hora = hora;
        this.minutos = minutos;
    }

    public long getId() {
        return id;
    }

    public void setId(long mID) {
        this.id = mID;
    }

    @Ignore
    public Partido (long ID, String equipoLocal, String equipoVisitante, String date, String horas, String minutos){
        this.id = ID;
        this.equipoLocal = equipoLocal;
        this.equipoVisitante = equipoVisitante;
        this.hora = Integer.parseInt(horas);
        this.minutos = Integer.parseInt(minutos);
        try {
            this.fechaPartido = Partido.FORMAT.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public Partido (long id, String equipoLocal, String equipoVisitante, Date fechaPartido, int hora, int minutos){
        this.id = id;
        this.equipoLocal = equipoLocal;
        this.equipoVisitante = equipoVisitante;
        this.hora = hora;
        this.minutos = minutos;
        this.fechaPartido = fechaPartido;
    }

    @Ignore
    public Partido (Intent intent){
        if(intent.getStringExtra(Partido.ID)!=null)
            id = Long.parseLong(intent.getStringExtra(Partido.ID));
        equipoLocal = intent.getStringExtra(Partido.EQUIPOLOCAL);
        equipoVisitante = intent.getStringExtra(Partido.EQUIPOVISITANTE);
        hora = Integer.valueOf(intent.getStringExtra(Partido.HORA));
        minutos = Integer.valueOf(intent.getStringExtra(Partido.MINUTOS));

        try {
            fechaPartido = Partido.FORMAT.parse(intent.getStringExtra(Partido.FECHAINICIO));
        } catch (ParseException e) {
            fechaPartido = new Date();
        }
    }

    public String getEquipoLocal() {
        return equipoLocal;
    }

    public void setEquipoLocal(String equipoLocal) {
        this.equipoLocal = equipoLocal;
    }

    public String getEquipoVisitante() {
        return equipoVisitante;
    }

    public void setEquipoVisitante(String equipoVisitante) {
        this.equipoVisitante = equipoVisitante;
    }

    public Date getFechaPartido() {
        return fechaPartido;
    }

    public void setFechaPartido(Date fechaPartido) {
        this.fechaPartido = fechaPartido;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }

    @Ignore
    public static void packageIntent (Intent intent, String equipoLocal, String equipoVisitante,
                                      String fechaInicio, String hora, String minutos){

        intent.putExtra(Partido.EQUIPOLOCAL, equipoLocal);
        intent.putExtra(Partido.EQUIPOVISITANTE, equipoVisitante);
        intent.putExtra(Partido.FECHAINICIO, fechaInicio);
        intent.putExtra(Partido.HORA, hora);
        intent.putExtra(Partido.MINUTOS, minutos);
    }

    @Ignore
    public static void packageIntent2 (Intent intent, long id, String equipoLocal, String equipoVisitante,
                                      String fechaInicio, String hora, String minutos){

        intent.putExtra(Partido.ID, String.valueOf(id));
        intent.putExtra(Partido.EQUIPOLOCAL, equipoLocal);
        intent.putExtra(Partido.EQUIPOVISITANTE, equipoVisitante);
        intent.putExtra(Partido.FECHAINICIO, fechaInicio);
        intent.putExtra(Partido.HORA, hora);
        intent.putExtra(Partido.MINUTOS, minutos);
    }

    @Ignore
    public String horaToString(){
        if(hora < 10){
            return "0"+hora;
        } else {
            return ""+hora;
        }
    }

    @Ignore
    public String minToString(){
        if(minutos<10){
            return "0"+minutos;
        } else {
            return ""+minutos;
        }
    }

    @Ignore
    public String toString(){
        return equipoLocal + ITEM_SEP + equipoVisitante + ITEM_SEP + FORMAT.format(fechaPartido)
                + ITEM_SEP + hora + ITEM_SEP + minutos;
    }


}
