package com.example.rauls.proyectoasee.RoomDatabase.Partidos;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {Partido.class}, version = 1)
public abstract class PartidosDatabase extends RoomDatabase {
    private static PartidosDatabase instance;

    public static PartidosDatabase getDatabase(Context context){
        if (instance == null)
            instance = Room.databaseBuilder(context.getApplicationContext(), PartidosDatabase.class, "matches.db").build();
        return instance;
    }

    public abstract PartidoDao partidoDao();
}
