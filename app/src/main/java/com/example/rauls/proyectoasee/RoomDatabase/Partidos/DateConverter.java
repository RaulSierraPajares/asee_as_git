package com.example.rauls.proyectoasee.RoomDatabase.Partidos;

import android.arch.persistence.room.TypeConverter;

import java.text.ParseException;
import java.util.Date;

public class DateConverter {

    @TypeConverter
    public static Date toDate (String fecha) {
        try {
            return fecha == null ? null : Partido.FORMAT.parse(fecha);
        } catch (ParseException e){
            e.printStackTrace();
        }
        return null;
    }

    @TypeConverter
    public static String toString(Date fecha){
        return fecha == null ? null : Partido.FORMAT.format(fecha);
    }
}
