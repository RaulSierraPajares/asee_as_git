package com.example.rauls.proyectoasee.RoomDatabase.Partidos;

import android.arch.persistence.room.TypeConverter;

public class MinConverter {
    @TypeConverter
    public static int toMin (String min) {
        return min == null ? null : Integer.valueOf(min);
    }

    @TypeConverter
    public static String toString(int min){
        return String.valueOf(min);
    }
}
