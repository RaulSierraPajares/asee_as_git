package com.example.rauls.proyectoasee.SimpleTypes;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.rauls.proyectoasee.RoomDatabase.Jugadores.Jugador;

import java.io.Serializable;
import java.util.List;

public class Equipo implements Serializable,
        Parcelable {

    public static String NAME = "name";
    public static String ADDRESS = "address";
    public static String PHONE = "phone";
    public static String WEBSITE = "website";
    public static String EMAIL = "email";
    public static String FOUNDEDYEAR = "year";
    public static String STADIUMNAME = "stadium";

    private String id;
    private String name;
    private String address;
    private String phone;
    private String webSite;
    private String email;
    private String foundedYear;
    private String stadiumName;
    private List<Jugador> squad;

    public Equipo(String name, String id){
        this.id = id;
        this.name = name;
    }

    public Equipo(String id, String name, String address, String phone,
                  String webSite, String email, String foundedYear,
                  String stadiumName, List<Jugador> squad) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.webSite = webSite;
        this.email = email;
        this.foundedYear = foundedYear;
        this.stadiumName = stadiumName;
        this.squad = squad;
    }

    public Equipo(String id, String name, String address, String phone,
                  String webSite, String email, String foundedYear,
                  String stadiumName) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.webSite = webSite;
        this.email = email;
        this.foundedYear = foundedYear;
        this.stadiumName = stadiumName;
    }

    public Equipo(Intent intent){
        this.name = intent.getStringExtra(Equipo.NAME);
        this.address = intent.getStringExtra(Equipo.ADDRESS);
        this.phone = intent.getStringExtra(Equipo.PHONE);
        this.webSite = intent.getStringExtra(Equipo.WEBSITE);
        this.email = intent.getStringExtra(Equipo.EMAIL);
        this.foundedYear = intent.getStringExtra(Equipo.FOUNDEDYEAR);
        this.stadiumName = intent.getStringExtra(Equipo.STADIUMNAME);
    }

    public static void packageIntent(Intent intent, String name, String address, String phone,
                                     String webSite, String email, String foundedYear,
                                     String stadiumName) {
        intent.putExtra(Equipo.NAME, name);
        intent.putExtra(Equipo.ADDRESS, address);
        intent.putExtra(Equipo.PHONE, phone);
        intent.putExtra(Equipo.WEBSITE, webSite);
        intent.putExtra(Equipo.EMAIL, email);
        intent.putExtra(Equipo.FOUNDEDYEAR, foundedYear);
        intent.putExtra(Equipo.STADIUMNAME, stadiumName);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFoundedYear() {
        return foundedYear;
    }

    public void setFoundedYear(String foundedYear) {
        this.foundedYear = foundedYear;
    }

    public String getStadiumName() {
        return stadiumName;
    }

    public void setStadiumName(String stadiumName) {
        this.stadiumName = stadiumName;
    }

    public Equipo(Parcel in){
        String [] data = new String [8];
        in.readStringArray(data);
        this.id = data[0];
        this.name = data[1];
        this.address = data[2];
        this.phone = data[3];
        this.webSite = data[4];
        this.email = data[5];
        this.foundedYear = data[6];
        this.stadiumName = data[7];
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String [] {this.id,
                                             this.name,
        this.address,
        this.phone,
        this.webSite,
        this.email,
        this.foundedYear,
        this.stadiumName});
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator(){

        @Override
        public Equipo createFromParcel(Parcel source) {
            return new Equipo(source);
        }

        @Override
        public Equipo[] newArray(int size) {
            return new Equipo[size];
        }
    };

    public List<Jugador> getSquad() {
        return squad;
    }

    public void setSquad(List<Jugador> squad) {
        this.squad = squad;
    }
}
