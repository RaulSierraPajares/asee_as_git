package com.example.rauls.proyectoasee.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.rauls.proyectoasee.R;

import java.util.ArrayList;
import java.util.List;

public class CountryFragment extends Fragment {
    private static final String URL = "http://api.football-data.org/v2/";

    private List<String> countries_list;

    private OnFragmentInteractionListener mListener;

    public CountryFragment() {
    }

    public static CountryFragment newInstance(String param1, String param2) {
        CountryFragment fragment = new CountryFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        countries_list = new ArrayList<String>();
        countries_list.add("España");
        countries_list.add("Inglaterra");
        countries_list.add("Francia");
        countries_list.add("Alemania");
        countries_list.add("Italia");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_country, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle saveInstanceState){
        ListView list = (ListView) getView().findViewById(R.id.country_list);
        ArrayAdapter adapter = new ArrayAdapter(getActivity(), R.layout.textview_search, countries_list) ;
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle args = new Bundle();
                String competition = "";
                String pais = "";
                switch (countries_list.get(position)){
                    case "Alemania":
                        competition="competitions/2002/teams";
                        pais = "Alemania";
                        break;
                    case "España":
                        competition="competitions/2014/teams";
                        pais = "España";
                        break;
                    case "Francia":
                        competition="competitions/2015/teams";
                        pais = "Francia";
                        break;
                    case "Inglaterra":
                        competition="competitions/2021/teams";
                        pais = "Inglaterra";
                        break;
                    case "Italia":
                        competition="competitions/2019/teams";
                        pais = "Italia";
                        break;
                    default:
                        break;
                }
//                Intent intent = new Intent(getActivity(), NetWorkingActivityTeams.class);
//                intent.putExtra("competition", competition);
//                getActivity().startActivity(intent);

                Bundle arguments = new Bundle();
                arguments.putString("competition", competition);
                arguments.putString("pais", pais);

                TeamsFragment tfr = new TeamsFragment();
                tfr.setArguments(arguments);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.contenedor, tfr).addToBackStack("").commit();
            }
        });
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
