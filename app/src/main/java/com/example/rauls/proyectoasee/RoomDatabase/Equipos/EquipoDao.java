package com.example.rauls.proyectoasee.RoomDatabase.Equipos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.rauls.proyectoasee.Database.DBContract;

import java.util.List;

@Dao
public interface EquipoDao {
//    @Query("SELECT * FROM "+DBContract.Equipo.TABLE_NAME)
//    public List<EquipoDB> getAll();
//
//    @Query("SELECT * FROM "+DBContract.Equipo.TABLE_NAME + " WHERE country" +" = :country")
//    public List<EquipoDB> getAllByCountry(String country);

    @Query("SELECT * FROM "+DBContract.Equipo.TABLE_NAME)
    public LiveData<List<EquipoDB>> getAll();

    @Query("SELECT * FROM "+DBContract.Equipo.TABLE_NAME + " WHERE country" +" = :country")
    public LiveData<List<EquipoDB>> getAllByCountry(String country);

    @Query("DELETE FROM "+DBContract.Equipo.TABLE_NAME+" WHERE country=:country")
    public void deleteAllCountry(String country);

    @Query("SELECT COUNT(*) FROM "+DBContract.Equipo.TABLE_NAME+" WHERE country=:country")
    public long count(String country);

    @Query("SELECT min(fechaAPI) FROM "+DBContract.Equipo.TABLE_NAME+" WHERE country=:country")
    public String fecha(String country);

    @Insert
    public long insert(EquipoDB equipo);

    @Update
    public void update(EquipoDB equipo);

    @Delete
    public void delete (EquipoDB equipo);
}
