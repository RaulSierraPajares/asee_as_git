package com.example.rauls.proyectoasee.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.rauls.proyectoasee.RoomDatabase.Partidos.Partido;

import java.util.ArrayList;
import java.util.List;

public final class MatchCRUD {
    private ToDoManagerDbHelper mDbHelper;
    private static MatchCRUD mInstance;

    private MatchCRUD(Context context){
        mDbHelper = new ToDoManagerDbHelper(context);
    }

    public static MatchCRUD getmInstance (Context context){
        if (mInstance == null){
            mInstance = new MatchCRUD(context);
        }
        return mInstance;
    }

    public List<Partido> getAll(){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String [] projection = {
                DBContract.Partido._ID,
                DBContract.Partido.COLUMN_LOCAL_TEAM,
                DBContract.Partido.COLUMN_VISITOR_TEAM,
                DBContract.Partido.COLUMN_DATE_MATCH,
                DBContract.Partido.COLUMN_HOUR_MATCH,
                DBContract.Partido.COLUMN_MINUTE_MATCH
        };

        String selection = null;
        String [] selectionArgs = null;

        String sortOrder = null;

        Cursor cursor = db.query(
                DBContract.Partido.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );

        ArrayList<Partido> partidos = new ArrayList<>();
        if(cursor.getCount() > 0){
            cursor.moveToFirst();
            do{
                partidos.add(getPartidoFromCursor(cursor));
            } while(cursor.moveToNext());
        }
        cursor.close();
        return partidos;
    }

    public long insert(Partido partido){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DBContract.Partido.COLUMN_LOCAL_TEAM, partido.getEquipoLocal());
        values.put(DBContract.Partido.COLUMN_VISITOR_TEAM, partido.getEquipoVisitante());
        values.put(DBContract.Partido.COLUMN_DATE_MATCH, Partido.FORMAT.format(partido.getFechaPartido()));
        values.put(DBContract.Partido.COLUMN_HOUR_MATCH, partido.getHora());
        values.put(DBContract.Partido.COLUMN_MINUTE_MATCH, partido.getMinutos());

        long newRowId = db.insert(DBContract.Partido.TABLE_NAME, null, values);
        return newRowId;
    }

    public void update(Partido partido){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBContract.Partido.COLUMN_LOCAL_TEAM, partido.getEquipoLocal());
        values.put(DBContract.Partido.COLUMN_VISITOR_TEAM, partido.getEquipoVisitante());
        values.put(DBContract.Partido.COLUMN_DATE_MATCH, Partido.FORMAT.format(partido.getFechaPartido()));
        values.put(DBContract.Partido.COLUMN_HOUR_MATCH, partido.getHora());
        values.put(DBContract.Partido.COLUMN_MINUTE_MATCH, partido.getMinutos());
        db.update(DBContract.Partido.TABLE_NAME, values, DBContract.Partido._ID + "=" + partido.getId(), null);
    }

    public void delete (Partido partido){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        db.delete(DBContract.Partido.TABLE_NAME, DBContract.Partido._ID + "=" + partido.getId(), null);
    }

    public void close(){
        if(mDbHelper!=null){
            mDbHelper.close();
        }
    }

    public static Partido getPartidoFromCursor(Cursor cursor){
        long ID = cursor.getInt(cursor.getColumnIndex(DBContract.Partido._ID));
        String localTeam = cursor.getString(cursor.getColumnIndex(DBContract.Partido.COLUMN_LOCAL_TEAM));
        String visitorTeam = cursor.getString(cursor.getColumnIndex(DBContract.Partido.COLUMN_VISITOR_TEAM));
        String dateMatch = cursor.getString(cursor.getColumnIndex(DBContract.Partido.COLUMN_DATE_MATCH));
        String hourMatch = cursor.getString(cursor.getColumnIndex(DBContract.Partido.COLUMN_HOUR_MATCH));
        String minuteMatch = cursor.getString(cursor.getColumnIndex(DBContract.Partido.COLUMN_MINUTE_MATCH));

        Partido partido = new Partido(ID,localTeam,visitorTeam,dateMatch,hourMatch,minuteMatch);
        return partido;
    }
}
