package com.example.rauls.proyectoasee.RoomDatabase.Partidos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.rauls.proyectoasee.Database.DBContract;

import java.util.List;

@Dao
public interface PartidoDao {
    @Query("SELECT * FROM "+DBContract.Partido.TABLE_NAME)
    public LiveData<List<Partido>> getAll();

    @Insert
    public long insert(Partido partido);

    @Update
    public void update(Partido partido);

    @Delete
    public void delete (Partido partido);
}
