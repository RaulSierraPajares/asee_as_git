package com.example.rauls.proyectoasee.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rauls.proyectoasee.R;
import com.example.rauls.proyectoasee.RoomDatabase.Equipos.EquipoDB;

public class InformationTeamFragment extends Fragment {
    private static final String ARG_PARAM1 = "id";
    private static final String ARG_PARAM2 = "equipo";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private EquipoDB mParam2;

    private OnFragmentInteractionListener mListener;

    public InformationTeamFragment() {
    }

    public static InformationTeamFragment newInstance(String param1, String param2) {
        InformationTeamFragment fragment = new InformationTeamFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam2 = (EquipoDB) getArguments().getSerializable(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_information_team, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onViewCreated(View view, Bundle saveInstanceState){
        TextView name = (TextView) getView().findViewById(R.id.insert_name);
        TextView address = (TextView) getView().findViewById(R.id.insert_address);
        TextView phone = (TextView) getView().findViewById(R.id.insert_phone);
        TextView email = (TextView) getView().findViewById(R.id.insert_email);
        TextView web = (TextView) getView().findViewById(R.id.insert_web);
        TextView year = (TextView) getView().findViewById(R.id.insert_year);
        TextView stadium = (TextView) getView().findViewById(R.id.insert_stadium);


        SharedPreferences sp = getActivity().getSharedPreferences(PreferenceManager.getDefaultSharedPreferencesName(getActivity()), getActivity().MODE_PRIVATE);
        int color = Integer.valueOf(sp.getString("color", "1"));
        name.setText(mParam2.getName());
        setColor(name,color);

        address.setText(mParam2.getAddress());
        setColor(address,color);

        phone.setText(mParam2.getPhone());
        setColor(phone,color);

        email.setText(mParam2.getEmail());
        setColor(email,color);

        web.setText(mParam2.getWebSite());
        setColor(web,color);

        year.setText(mParam2.getFoundedYear());
        setColor(year,color);

        stadium.setText(mParam2.getStadiumName());
        setColor(stadium,color);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void setColor (TextView v, int color){
        switch (color){
            case 1:
                v.setTextColor(ContextCompat.getColor(getActivity(), R.color.transfermarkt));
                break;
            case 2:
                v.setTextColor(ContextCompat.getColor(getActivity(), R.color.rojo));
                break;
            case 3:
                v.setTextColor(ContextCompat.getColor(getActivity(), R.color.verde));
                break;
            case 4:
                v.setTextColor(ContextCompat.getColor(getActivity(), R.color.naranja));
                break;
            default:
                break;
        }
    }
}
