package com.example.rauls.proyectoasee.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.rauls.proyectoasee.ManagementAPI.NetworkingAPITeamSquad;
import com.example.rauls.proyectoasee.RoomDatabase.Jugadores.Jugador;
import com.example.rauls.proyectoasee.RoomDatabase.Jugadores.JugadorDao;
import com.example.rauls.proyectoasee.RoomDatabase.Jugadores.JugadorDatabase;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class JugadorRepository {
    private static JugadorRepository instance;
    private JugadorDao mJugadordao;
    private List<Jugador> jugadoresDB;
    private List<Jugador> jugadoresAPI;
    private long umbral =  432000000; //Cinco dias

    public JugadorRepository (Application app){
        JugadorDatabase db = JugadorDatabase.getDatabase(app);
        mJugadordao = db.jugadorDao();
    }

    public static JugadorRepository getInstance(Application app){
        if(instance == null){
            instance = new JugadorRepository(app);
        }
        return instance;
    }

    public void insert (Jugador jugador){
        new JugadorRepository.insertAsynTask(mJugadordao).execute(jugador);
    }

    public List<Jugador> getAllByTeam(String team, String idTeam){
        try {
            jugadoresDB = new getAllByTeamAsyncTask(mJugadordao).execute(team).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Date current = new Date();
        if(jugadoresDB == null || jugadoresDB.isEmpty() || current.getTime() - jugadoresDB.get(0).getFechaAPI().getTime() > umbral){
            if(jugadoresDB != null && !jugadoresDB.isEmpty() && current.getTime() - jugadoresDB.get(0).getFechaAPI().getTime() > umbral){
                new deleteAllByTeamAsyncTask(mJugadordao).execute(team);
            }
            try {
                jugadoresAPI =  new NetworkingAPITeamSquad.HttpGetTask().execute(idTeam).get();
                for(int i = 0; i<jugadoresAPI.size(); i++){
                    jugadoresAPI.get(i).setFechaAPI(current);
                    insert(jugadoresAPI.get(i));
                }
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                jugadoresDB = new getAllByTeamAsyncTask(mJugadordao).execute(team).get();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return jugadoresDB;
    }

    public LiveData<Jugador> getJugador (String equipo, String id){
        try {
            return new getJugadorAsyncTask(mJugadordao).execute(equipo, id).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static class insertAsynTask extends AsyncTask<Jugador, Void, Void> {
        private JugadorDao mAsynTaskDao;

        insertAsynTask(JugadorDao dao){
            mAsynTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Jugador... items) {
            long id = mAsynTaskDao.insert(items[0]);
            return null;
        }
    }

    private static class getAllByTeamAsyncTask extends AsyncTask<String, Void, List<Jugador>>{
        private JugadorDao mAsyncTaskDao;

        getAllByTeamAsyncTask(JugadorDao dao){
            mAsyncTaskDao = dao;
        }

        @Override
        protected List<Jugador> doInBackground(String... items) {
            return mAsyncTaskDao.getAllByTeam(items[0]);
        }
    }

    private static class deleteAllByTeamAsyncTask extends AsyncTask<String, Void, Void>{
        private JugadorDao mAsyncTaskDao;

        deleteAllByTeamAsyncTask(JugadorDao dao){
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(String... items) {
            mAsyncTaskDao.deleteAllByTeam(items[0]);
            return null;
        }
    }

    private static class getJugadorAsyncTask extends AsyncTask<String, Void, LiveData<Jugador>>{
        JugadorDao mAsyncDao;
        getJugadorAsyncTask(JugadorDao dao){
            mAsyncDao = dao;
        }
        @Override
        protected LiveData<Jugador> doInBackground(String... strings) {
            return mAsyncDao.getJugador(strings[0], strings[1]);
        }
    }
}
