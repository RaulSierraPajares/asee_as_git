package com.example.rauls.proyectoasee.RoomDatabase.Equipos;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {EquipoDB.class}, version = 1)
public abstract class EquipoDatabase extends RoomDatabase {
    private static EquipoDatabase instance;

    public static EquipoDatabase getDatabase(Context context){
        if (instance == null)
            instance = Room.databaseBuilder(context.getApplicationContext(), EquipoDatabase.class, "equipos.db").build();
        return instance;
    }

    public abstract EquipoDao equipoDao();
}
