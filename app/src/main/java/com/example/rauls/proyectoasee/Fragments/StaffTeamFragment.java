package com.example.rauls.proyectoasee.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.rauls.proyectoasee.Activities.DetailsPlayerStaffActivity;
import com.example.rauls.proyectoasee.R;
import com.example.rauls.proyectoasee.RoomDatabase.Jugadores.Jugador;

import java.util.ArrayList;
import java.util.List;

public class StaffTeamFragment extends Fragment {
    private static final String ARG_PARAM1 = "staff";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private List<Jugador> mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public StaffTeamFragment() {
    }

    public static StaffTeamFragment newInstance(String param1, String param2) {
        StaffTeamFragment fragment = new StaffTeamFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = (List<Jugador>) getArguments().getSerializable(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_staff_team, container, false);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle saveInstanceState){
        ListView staff = (ListView) getView().findViewById(R.id.list_staff);
        ArrayAdapter adapter = new ArrayAdapter(getActivity(), R.layout.textview_search, getNames(mParam1)) ;
        staff.setAdapter(adapter);
        staff.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), DetailsPlayerStaffActivity.class);
                intent.putExtra("equipo_nombre", mParam1.get(position).getTeam());
                intent.putExtra("identificador", mParam1.get(position).getId());
                getActivity().startActivity(intent);
            }
        });
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private ArrayList<String> getNames (List<Jugador> lj){
        ArrayList<String> aux = new ArrayList<>();
        for(int i = 0; i<lj.size(); i++){
            aux.add(lj.get(i).getName());
        }
        return aux;
    }
}
