package com.example.rauls.proyectoasee.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.rauls.proyectoasee.Repository.JugadorRepository;
import com.example.rauls.proyectoasee.RoomDatabase.Jugadores.Jugador;

public class JugadorViewModel extends AndroidViewModel {
    private JugadorRepository mJugadorRepository;
    private LiveData<Jugador> mJugador;

    public JugadorViewModel(@NonNull Application application) {
        super(application);
        mJugadorRepository = JugadorRepository.getInstance(application);
    }

    public LiveData<Jugador> getJugador(String equipo, String id){
        return mJugadorRepository.getJugador(equipo, id);
    }
}
