package com.example.rauls.proyectoasee.RoomDatabase.Equipos;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.example.rauls.proyectoasee.Database.DBContract;
import com.example.rauls.proyectoasee.RoomDatabase.Partidos.DateConverter;

import java.io.Serializable;
import java.util.Date;

@Entity(tableName = DBContract.Equipo.TABLE_NAME)
public class EquipoDB implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private long id;
    private String name;
    private String address;
    private String phone;
    private String webSite;
    private String email;
    private String foundedYear;
    private String stadiumName;
    private String country;
    @TypeConverters(DateConverter.class)
    private Date fechaAPI;

    public String getIdAPI() {
        return idAPI;
    }

    public void setIdAPI(String idAPI) {
        this.idAPI = idAPI;
    }

    private String idAPI;

    public EquipoDB(long id, String name, String address, String phone,
                    String webSite, String email, String foundedYear,
                    String stadiumName, String country, Date fechaAPI, String idAPI) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.webSite = webSite;

        this.email = email;
        this.foundedYear = foundedYear;
        this.stadiumName = stadiumName;
        this.country = country;

        this.fechaAPI = fechaAPI;
        this.idAPI = idAPI;
    }

    @Ignore
    public EquipoDB(String name, String address, String phone,
                    String webSite, String email, String foundedYear,
                    String stadiumName, String country, Date fechaAPI, String idAPI) {
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.webSite = webSite;

        this.email = email;
        this.foundedYear = foundedYear;
        this.stadiumName = stadiumName;
        this.country = country;

        this.fechaAPI = fechaAPI;
        this.idAPI = idAPI;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFoundedYear() {
        return foundedYear;
    }

    public void setFoundedYear(String foundedYear) {
        this.foundedYear = foundedYear;
    }

    public String getStadiumName() {
        return stadiumName;
    }

    public void setStadiumName(String stadiumName) {
        this.stadiumName = stadiumName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getFechaAPI() {
        return fechaAPI;
    }

    public void setFechaAPI(Date fechaAPI) {
        this.fechaAPI = fechaAPI;
    }

}

