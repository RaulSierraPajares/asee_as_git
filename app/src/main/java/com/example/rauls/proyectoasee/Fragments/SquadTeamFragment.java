package com.example.rauls.proyectoasee.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.rauls.proyectoasee.Activities.DetailsPlayerStaffActivity;
import com.example.rauls.proyectoasee.R;
import com.example.rauls.proyectoasee.RoomDatabase.Jugadores.Jugador;

import java.util.ArrayList;
import java.util.List;

public class SquadTeamFragment extends Fragment {
    private static final String ARG_PARAM1 = "id";
    private static final String ARG_PARAM2 = "jugadores";

    private String mParam1;
    private List<Jugador> mParam2;

    private OnFragmentInteractionListener mListener;

    public SquadTeamFragment() {
    }

    public static SquadTeamFragment newInstance(String param1, String param2) {
        SquadTeamFragment fragment = new SquadTeamFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam2 = (List<Jugador>) getArguments().getSerializable(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_squad_team, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onViewCreated(View view, Bundle saveInstanceState){
        ListView players = (ListView) getView().findViewById(R.id.list_players);
        SharedPreferences sp = getActivity().getSharedPreferences(PreferenceManager.getDefaultSharedPreferencesName(getActivity()), getActivity().MODE_PRIVATE);
        int max = Integer.valueOf(sp.getString("numJugadores", "0"));
        ArrayAdapter adapter = new ArrayAdapter(getActivity(), R.layout.textview_search, getNames(mParam2, max)) ;
        players.setAdapter(adapter);
        players.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), DetailsPlayerStaffActivity.class);
                intent.putExtra("equipo_nombre", mParam2.get(position).getTeam());
                intent.putExtra("identificador", mParam2.get(position).getId());
                getActivity().startActivity(intent);
            }
        });
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private ArrayList<String> getNames (List<Jugador> lj, int max){
        ArrayList<String> aux = new ArrayList<>();
        int cont;
        if(max == 1){
            if(lj == null){
                cont = 0;
            } else {
                cont = lj.size();
            }
        } else {
            cont = max;
        }
        for(int i = 0; i<cont; i++){
                aux.add(lj.get(i).getName());
        }
        return aux;
    }
}
