package com.example.rauls.proyectoasee.Fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.example.rauls.proyectoasee.Activities.DetailsTeamActivity;
import com.example.rauls.proyectoasee.R;
import com.example.rauls.proyectoasee.RoomDatabase.Equipos.EquipoDB;
import com.example.rauls.proyectoasee.ViewModel.EquiposViewModel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class TeamsFragment extends Fragment {
    private static final String BASE_URL = "http://api.football-data.org/v2/competitions/";
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    // TODO: Rename and change types of parameters
    private String competition;
    private String pais;

    private EquiposViewModel mEquiposViewModel;
    private List<EquipoDB> equiposDB;
    private ListAdapter adapter;

    private OnFragmentInteractionListener mListener;

    public TeamsFragment() {
    }

    public static TeamsFragment newInstance(String param1, String param2) {
        TeamsFragment fragment = new TeamsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            competition = getArguments().getString("competition");
            pais = getArguments().getString("pais");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_teams, container, false);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onViewCreated(View view, Bundle saveInstanceState){
        final ListView list = (ListView) getView().findViewById(R.id.teamList);

        SharedPreferences sp = getActivity().getSharedPreferences(PreferenceManager.getDefaultSharedPreferencesName(getActivity()), getActivity().MODE_PRIVATE);
        final int max = Integer.valueOf(sp.getString("numEquipos", "0"));

        mEquiposViewModel = ViewModelProviders.of(this).get(EquiposViewModel.class);
        mEquiposViewModel.getAllEquipos(pais, competition).observe(this, new Observer<List<EquipoDB>>() {
            @Override
            public void onChanged(@Nullable List<EquipoDB> equipoDBS) {
                final List<EquipoDB> aux2 = equipoDBS;
                if(equipoDBS != null && !equipoDBS.isEmpty()){
                    List<String> soloNombres;
                    if(max == 1){
                        soloNombres = soloNombres(equipoDBS, equipoDBS.size());
                    } else {
                        soloNombres = soloNombres(equipoDBS, max);
                    }
                    adapter = new ArrayAdapter(getActivity(), R.layout.textview_search, soloNombres);
                    list.setAdapter(adapter);
                    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent = new Intent(getActivity(), DetailsTeamActivity.class);
                            intent.putExtra("equipo", aux2.get(position));
                            getActivity().startActivity(intent);
                        }
                    });
                } else {
                    ArrayList<EquipoDB> aux = new ArrayList<>();
                    ListAdapter adapter = new ArrayAdapter(getActivity(), R.layout.textview_search, aux);
                    list.setAdapter(adapter);
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public static List<String> soloNombres (List<EquipoDB> equipos, int max){
        List <String> nombres = new ArrayList<>();
        for(int i = 0; i<max; i++){
            nombres.add(equipos.get(i).getName());
        }
        return nombres;
    }

    public static String slurp(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line+"\n");
        }
        br.close();
        return sb.toString();
    }
}
