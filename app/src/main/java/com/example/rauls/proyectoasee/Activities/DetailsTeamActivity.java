package com.example.rauls.proyectoasee.Activities;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.example.rauls.proyectoasee.Fragments.InformationTeamFragment;
import com.example.rauls.proyectoasee.Fragments.SquadTeamFragment;
import com.example.rauls.proyectoasee.Fragments.StaffTeamFragment;
import com.example.rauls.proyectoasee.R;
import com.example.rauls.proyectoasee.Repository.JugadorRepository;
import com.example.rauls.proyectoasee.RoomDatabase.Equipos.EquipoDB;
import com.example.rauls.proyectoasee.SimpleTypes.Equipo;
import com.example.rauls.proyectoasee.RoomDatabase.Jugadores.Jugador;

import java.util.ArrayList;
import java.util.List;

public class DetailsTeamActivity extends AppCompatActivity implements ActionBar.TabListener,
        ViewPager.OnPageChangeListener,
        SquadTeamFragment.OnFragmentInteractionListener,
        InformationTeamFragment.OnFragmentInteractionListener,
        StaffTeamFragment.OnFragmentInteractionListener {

    private SectionsPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;
    private Equipo team;

    private EquipoDB equipoDB;
    private JugadorRepository mRepository;
    private List<Jugador> jugadores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_team);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

//        if(getIntent().getBundleExtra("bundle") != null){
//            if(getIntent().getBundleExtra("bundle").getSerializable("equipo") != null){
//                team = (Equipo) getIntent().getBundleExtra("bundle").getSerializable("equipo");
//                jugadores = (ArrayList<Jugador>) getIntent().getBundleExtra("bundle").getSerializable("jugadores");
//                toolbar.setTitle(team.getName());
//            }
//        } else {
//
//        }

        if(getIntent().getSerializableExtra("equipo") != null){
            Log.i("Cambio", "Obteniendo el equipo a mostrar");
            equipoDB = (EquipoDB) getIntent().getSerializableExtra("equipo");
            Log.i("Cambio", "El equipo es: "+equipoDB.getName());
            toolbar.setTitle(equipoDB.getName());

            mRepository = JugadorRepository.getInstance(getApplication());
            jugadores = mRepository.getAllByTeam(equipoDB.getName(), equipoDB.getIdAPI());
        }

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
//        savedInstanceState.putSerializable("equipo", team);
//        savedInstanceState.putSerializable("jugadores", jugadores);

        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        getSupportActionBar().setSelectedNavigationItem(i);
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment tabFragment = null;
            Bundle args = new Bundle();
            switch (position){
                case 0:
                    tabFragment = new InformationTeamFragment();
                    args.putSerializable("equipo", equipoDB);
                    tabFragment.setArguments(args);
                    break;
                case 1:
                    tabFragment = new SquadTeamFragment();
                    args.putSerializable("jugadores", getPlayers(jugadores));
                    tabFragment.setArguments(args);
                    break;
                case 2:
                    tabFragment = new StaffTeamFragment();
                    args.putSerializable("staff", getNotPlayers(jugadores));
                    tabFragment.setArguments(args);
                    break;
            }
            return tabFragment;
        }

        @Override
        public int getCount() {
            return 3;
        }

        private ArrayList<Jugador> getPlayers (List<Jugador> lj){
            ArrayList<Jugador> aux = new ArrayList<>();
            Jugador jaux;
            for(int i = 0; i<lj.size(); i++){
                if(!(jaux = lj.get(i)).getPosition().equals("No consta")){
                    aux.add(jaux);
                }
            }
            return aux;
        }

        private ArrayList<Jugador> getNotPlayers (List<Jugador> lj){
            ArrayList<Jugador> aux = new ArrayList<>();
            Jugador jaux;
            for(int i = 0; i<lj.size(); i++){
                if((jaux = lj.get(i)).getPosition().equals("No consta")){
                    aux.add(jaux);
                }
            }
            return aux;
        }

    }
}
