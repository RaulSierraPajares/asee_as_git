package com.example.rauls.proyectoasee.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.rauls.proyectoasee.Repository.EquipoRepository;
import com.example.rauls.proyectoasee.RoomDatabase.Equipos.EquipoDB;

import java.util.List;

public class EquiposViewModel extends AndroidViewModel {
    private EquipoRepository mEquiposRepository;

    private LiveData<List<EquipoDB>> mEquipos;

    public EquiposViewModel(@NonNull Application application) {
        super(application);
        mEquiposRepository = EquipoRepository.getRepository(application);
    }

    public LiveData<List<EquipoDB>> getAllEquipos(String pais, String competicion){
        return mEquiposRepository.getAllByCountry(pais, competicion);
    }


}
